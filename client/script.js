//local
const firebaseConfig = {
  apiKey: "AIzaSyAxAdNfoJjAX51LoDS6pvK9drDwVlhwcBI",
  authDomain: "core-product.firebaseapp.com",
  databaseURL: "https://core-product.firebaseio.com",
  projectId: "core-product",
  storageBucket: "core-product.appspot.com",
  messagingSenderId: "632059853605",
  appId: "1:632059853605:web:365907b7dd1f364a3ff8cc",
  measurementId: "G-VWVK1KWLRZ"
};

firebase.initializeApp(firebaseConfig);

const messaging = firebase.messaging();
// [END get_messaging_object]
// [START set_public_vapid_key]
// Add the public key generated from the console here.
messaging.usePublicVapidKey(
  'BPsB4ALkBSruKhaviBmNTGuYwtrBZOlFdpY4_sHmeHyW3eVVde1Z1sd5PbOZOwHxusKankEPwK6cvd4v9ZuhjM4', //local
  // 'BCP7geoI01jejMswMMZfB7CdigsSKprkZ8Q5UckasyIp0Wq1OgRCwX_gVKLYu-WCbdGeIa_ookwCf0cyhj8Q_i8',//dev
);
// [END set_public_vapid_key]
// IDs of divs that display Instance ID token UI or request permission UI.
const tokenDivId = 'token_div';
// [START refresh_token]
// Callback fired if Instance ID token is updated.
messaging.onTokenRefresh(() => {
  messaging
    .getToken()
    .then(refreshedToken => {
      console.log('Token refreshed.');
      // Indicate that the new Instance ID token has not yet been sent to the
      // app server.
      setTokenSentToServer(false);
      // Send Instance ID token to app server.
      sendTokenToServer(refreshedToken);
      // [START_EXCLUDE]
      // Display new Instance ID token and clear UI of all previous messages.
      resetUI();
      // [END_EXCLUDE]
    })
    .catch(err => {
      console.log('Unable to retrieve refreshed token ', err);
      showToken('Unable to retrieve refreshed token ', err);
    });
});
// [END refresh_token]
// [START receive_message]
// Handle incoming messages. Called when:
// - a message is received while the app has focus
// - the user clicks on an app notification created by a service worker
//   `messaging.setBackgroundMessageHandler` handler.
messaging.onMessage(payload => {
  toastr.clear();
  console.log('Message received. ', payload);
  // [START_EXCLUDE]
  // Update the UI to include the received message.
  appendMessage(payload);
  toastr.success(payload.notification.title, payload.notification.body, {timeOut: 5000});
  toastr.success(payload.data.title, payload.data.body);
  // [END_EXCLUDE]
});
// [END receive_message]
function resetUI() {
  clearMessages();
  showToken('loading...');
  // [START get_token]
  // Get Instance ID token. Initially this makes a network call, once retrieved
  // subsequent calls to getToken will return from cache.
  messaging
    .getToken()
    .then(currentToken => {
      if (currentToken) {
        sendTokenToServer(currentToken);
        updateUIForPushEnabled(currentToken);
      } else {
        // Show permission request.
        console.log(
          'No Instance ID token available. Request permission to generate one.',
        );
        // Show permission UI.
        updateUIForPushPermissionRequired();
        setTokenSentToServer(false);
      }
    })
    .catch(err => {
      console.log('An error occurred while retrieving token. ', err);
      showToken('Error retrieving Instance ID token. ', err);
      setTokenSentToServer(false);
    });
  // [END get_token]
}
function showToken(currentToken) {
  // Show token in console and UI.
  const tokenElement = document.querySelector('#token');
  tokenElement.textContent = currentToken;
}
// Send the Instance ID token your application server, so that it can:
// - send messages back to this app
// - subscribe/unsubscribe the token from topics
function sendTokenToServer(currentToken) {
  if (!isTokenSentToServer()) {
    console.log('Sending token to server...');
    // TODO(developer): Send the current token to your server.
    axios({
      method: 'put',
      url: 'http://localhost:3000/users/me/token/add',
      headers: {
        'Content-Type': 'application/json',
        'Authorization':'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySUQiOiJmMTJkMzlmYy1kMDczLTQ4NzAtYWU4Ni1kNjdhMzQ0NWU4OWUiLCJjaGFuZ2VQYXNzd29yZEF0IjpudWxsLCJzdGF0dXMiOiJBQ1RJVkUiLCJhY2Nlc3MiOlt7InJvbGVJRCI6IjQ2MTIwMjgxLTk1MTUtNDBhYi1hNTdkLTMzYmI3YjhhZDcwOCIsImNvbXBhbnlJRCI6bnVsbCwiZGVwYXJ0bWVudElEIjpudWxsfV0sImlhdCI6MTU5NTQwNzUxMywiZXhwIjoxNjEwOTU5NTEzfQ.x5EWhHOe0Cg1bQXXIXY6XssyRRItODmcomlXOzCinLc',
      },
      data: {
        token: currentToken,
      },
    });
    setTokenSentToServer(true);
  } else {
    console.log(
      "Token already sent to server so won't send it again " +
        'unless it changes',
    );
  }
}
function isTokenSentToServer() {
  return window.localStorage.getItem('sentToServer') === '1';
}
function setTokenSentToServer(sent) {
  window.localStorage.setItem('sentToServer', sent ? '1' : '0');
}
function showHideDiv(divId, show) {
  const div = document.querySelector('#' + divId);
  if (show) {
    div.style = 'display: visible';
  } else {
    div.style = 'display: none';
  }
}
function requestPermission() {
  console.log('Requesting permission...');
  // [START request_permission]
  Notification.requestPermission().then(permission => {
    if (permission === 'granted') {
      console.log('Notification permission granted.');
      // TODO(developer): Retrieve an Instance ID token for use with FCM.
      // [START_EXCLUDE]
      // In many cases once an app has been granted notification permission,
      // it should update its UI reflecting this.
      resetUI();
      // [END_EXCLUDE]
    } else {
      console.log('Unable to get permission to notify.');
    }
  });
  // [END request_permission]
}
function deleteToken() {
  // Delete Instance ID token.
  // [START delete_token]
  messaging
    .getToken()
    .then(currentToken => {
      messaging
        .deleteToken(currentToken)
        .then(() => {
          console.log('Token deleted.');
          setTokenSentToServer(false);
          // [START_EXCLUDE]
          // Once token is deleted update UI.
          resetUI();
          // [END_EXCLUDE]
        })
        .catch(err => {
          console.log('Unable to delete token. ', err);
        });
      // [END delete_token]
    })
    .catch(err => {
      console.log('Error retrieving Instance ID token. ', err);
      showToken('Error retrieving Instance ID token. ', err);
    });
}
// Add a message to the messages element.
function appendMessage(payload) {
  const messagesElement = document.querySelector('#messages');
  const dataHeaderELement = document.createElement('h5');
  const dataElement = document.createElement('pre');
  dataElement.style = 'overflow-x:hidden;';
  dataHeaderELement.textContent = 'Received message:';
  dataElement.textContent = JSON.stringify(payload, null, 2);
  messagesElement.appendChild(dataHeaderELement);
  messagesElement.appendChild(dataElement);
}
// Clear the messages element of all children.
function clearMessages() {
  const messagesElement = document.querySelector('#messages');
  while (messagesElement.hasChildNodes()) {
    messagesElement.removeChild(messagesElement.lastChild);
  }
}
function updateUIForPushEnabled(currentToken) {
  showHideDiv(tokenDivId, true);
  showToken(currentToken);
}
function updateUIForPushPermissionRequired() {
  showHideDiv(tokenDivId, false);
}
resetUI();
